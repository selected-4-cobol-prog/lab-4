       IDENTIFICATION DIVISION.
       PROGRAM-ID. BMICALCULATE.
       AUTHOR. Nithiphat.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  HEIGHT PIC 999v9.
       01  WEIGHT PIC 999v9.
       01  BMI PIC 99v99 VALUE ZEROS.
           88 UNDERWEIGHT       VALUE 0      THRU 18.5.
           88 NORMAL            VALUE 18.6   THRU 24.9.
           88 OVERWEIGHT        VALUE 25     THRU 29.9.
           88 OBESE             VALUE 30     THRU 34.9.
           88 EXTREMLY_OBESE    VALUE 35     THRU 100.
       01  CRITERION PIC X(35) VALUE SPACES.
           88  RATING-UNDERWEIGHT     VALUE "UNDERWEIGHT".
           88  RATING-NORMAL          VALUE "NORMAL".
           88  RATING-OVERWEIGHT      VALUE "OVERWEIGHT".
           88  RATING-OBESE           VALUE "OBESE".
           88  RATING-EXTREMLY-OBESE  VALUE "EXTREMLY OBESE".

       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY "Please input your height(cm): " WITH NO ADVANCING.
           ACCEPT HEIGHT . *> เก็บตัวแปร ค่าส่วนสูง
           DISPLAY "Also input your weight(kg): " WITH NO ADVANCING.
           ACCEPT WEIGHT . *>เก็บตัวแปร ค่าน้ำหนัก
           COMPUTE HEIGHT = HEIGHT  / 100.
           COMPUTE BMI ROUNDED = WEIGHT  / (HEIGHT * HEIGHT).
           DISPLAY "Your BMI is: " BMI.

           EVALUATE TRUE  
           WHEN  UNDERWEIGHT    SET RATING-UNDERWEIGHT     TO TRUE
           WHEN  NORMAL         SET RATING-NORMAL          TO TRUE
           WHEN  OVERWEIGHT     SET RATING-OVERWEIGHT      TO TRUE
           WHEN  OBESE          SET RATING-OBESE           TO TRUE
           WHEN  EXTREMLY_OBESE SET RATING-EXTREMLY-OBESE  TO TRUE

           END-EVALUATE
           DISPLAY "CRITERION WEIGHT RATING IS ---> " CRITERION
           GOBACK
           .
